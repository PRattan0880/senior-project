package main

import (
	"bytes"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestToken(t *testing.T) {
	token, resp := eagleViewToken()
	assert.NotNil(t, token, "`token` cant be nil")
	assert.Equal(t, "200 OK", resp, "invalid response")
}


func TestPlaceOrder(t *testing.T) {
	token, _ := eagleViewToken()
	var address = Address{"7104 Lyndale Circle", "Elk Grove", "CA", "95758", "Basic", "", "Jane", "Doe"}
	var credit = PaymentInfo{6, 2023, "", 1}
	orderStats, resp := placeOrder(token, address, credit)
	assert.NotNil(t, orderStats, "`orderStats` cant be nil")
	assert.Equal(t, "200 OK", resp, resp)
}

func TestRetrieveFile(t *testing.T) {
	token, _ := eagleViewToken()
	data, length, resp := getReportFile(token, "45778779", 75, 2)
	assert.NotNil(t, data, "`File Data` cant be nil")
	assert.Greater(t, length, int64(10), "`File Data Length cant be 0")
	assert.Equal(t, "200 OK", resp, resp)
}

func TestRetrieveImage(t *testing.T) {
	imageType := [5]int{6, 22, 23, 24, 25}
	token, _ := eagleViewToken()
	for i := 0; i < len(imageType); i++ {
		data, resp := getReportImage(token, "45778779", imageType[i])
		assert.NotNil(t, data, "`Image Data` cant be nil")
		assert.Equal(t, "200 OK", resp, resp)
	}
}

func TestConnectDB(t *testing.T) {
	db, err := connectDb()
	assert.NotNil(t, db, "`data` cant be nil")
	assert.Nil(t, err, "`No Connection Error`clear")
}

func TestRetrieveNRELData(t *testing.T) {
	var address = Address{"810 eagle ridge circle", "folsom", "CA", "95630", "Basic", "JaneDoe@gmail.com", "jane", "Doe"}
	data, resp := NRELData(address)
	assert.NotNil(t, data, "`data` cant be nil")
	assert.Equal(t, "200 OK", resp, resp)
}

func TestCheckReport(t *testing.T) {
	token, _ := eagleViewToken()
	data, resp := checkReport(token, "45778779")
	assert.NotNil(t, data, "`data` cant be nil")
	assert.Equal(t, "200 OK", resp, resp)
}

func TestCheckDB(t *testing.T) {
	db, _ := connectDb()
	var input = input{"45778779"}
	result := checkDb(db, input)
	assert.Equal(t, true, result.Valid, "`Query result can't be invalid`")
	assert.NotNil(t, result.String, "`Query Result reportId can't be empty`")
}

func TestRetrieveData(t *testing.T) {
	db, _ := connectDb()
	result := retrieveData(db, "45778779")
	assert.NotEmpty(t, result, "`Query Result address can't be empty`")
}

func TestDisplayImage(t *testing.T) {
	token, _ := eagleViewToken()
	imageData, _ := getReportImage(token, "45778779", 6)
	assert.NotNil(t, imageData, "`image can't be nil`")
	data, err := DisplayImage(imageData)
	assert.NotNil(t, data, "`Image Data can't be nil`")
	assert.Nil(t, err, "`PNG` can't be nil")

}

func TestDownloadPDF(t *testing.T) {
	token, _ := eagleViewToken()
	data, length, _ := getReportFile(token, "45778779", 75, 2)
	req, _ := http.NewRequest("GET", "/downloadPDF", bytes.NewReader(data))
	assert.NotNil(t, req, "`request` can't be nil")
	assert.Greater(t, int(length), 50, "`data` can't be nil")

}

func TestCheckDownloadReport(t *testing.T) {
	token, _ := eagleViewToken()
	url, resp := downloadReport(token, "45778779")
	assert.NotNil(t, url, "`request` can't be nil")
	assert.Equal(t, "200 OK", resp, resp)
}

func TestGetJsonFile(t *testing.T) {
	token, _ := eagleViewToken()
	url, _ := downloadReport(token, "45778779")
	err := getJsonFile(url, "45778779")
	assert.Nil(t, err, "`JSON Data` can't be nil")
}

func TestUnmarshalJSON(t *testing.T) {
	var reportId string = "45778779"
	jsonData, _ := os.ReadFile("RadianceModel" + reportId + ".json")
	var tab ExportEV
	test := unmarshalJSON(jsonData, tab)
	assert.NotNil(t, test, "`JSON Data` can't be nil")
	assert.NotNil(t, jsonData, "`JSON Data` can't be nil")
}
